﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallStop : MonoBehaviour {

    public Rigidbody2D ball;
    public BallController ballControl;
    private GameManager gameManager;

	// Use this for initialization
	void Start () {
        gameManager = FindObjectOfType<GameManager>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter2D(Collision2D other) {
        if(other.gameObject.tag == "Ball") {
            //Stop the ball
            ball.velocity = Vector2.zero;
            //Reset the level
            //set the ball as active
            ballControl.currentBallState = BallController.ballState.wait; // wait for other balls to land then shoot again
        }
        if (other.gameObject.tag == "Extra Ball") {
            gameManager.ballsInScene.Remove(other.gameObject);
            other.gameObject.SetActive(false);
        }
    }
}
