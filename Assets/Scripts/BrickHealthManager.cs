﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrickHealthManager : MonoBehaviour {

    public int brickHealth;
    private Text brickHealthText;
    private GameManager gameManager;
  

	// Use this for initialization
	void Start () {
        gameManager = FindObjectOfType<GameManager>(); // created a reference to the GameManage class
        brickHealth = gameManager.level; // the number on the brick will depend on the level
        brickHealthText = GetComponentInChildren<Text> ();
	}
	
	// Update is called once per frame
	void Update () {
        brickHealthText.text = "" + brickHealth;
        if(brickHealth <= 0)
        {
            //Destroy Brick
            this.gameObject.SetActive(false); // if the number on the bricks get to 0, then it disappear
        }
	}

    void TakeDamage(int damageToTake)
    {
        brickHealth -= damageToTake;
    }

    void OnCollisionExit2D(Collision2D other) // Method to minus bricks numbers when the ball hits them
    {
        if(other.gameObject.tag == "Ball") //if the ball hits the bricks, minus 1 
        {
            TakeDamage(1);  
        }
        
    }
}
